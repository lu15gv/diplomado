//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
/*
 LLVM Low Level virtual machine
 ABI Application Binary Interface
 */
var cadena = "asd"
//cadena = 8   --> Tipado seguro (no podemos cambniar tipo de dato)
//print(type(of: cadena))

// dump("asd")

struct Person{
    let name:String
    let age:Int
}

let saludar = Person(name: "Jose", age:13)
dump(saludar)


//Shift left <<
1 << 3 //desplazamineto de bits
//Shift right >>
1 >> 3

(3).squareRoot()

//Declaración de datos
var numero : CGFloat = 3


var contador : Int = 0
contador += 2
contador -= 3
contador *= 9
contador /= -3

var letra : Character = "A"

//Interpolacion
print("hola \(letra)")

let cadenota = """
Hola
Como
estas?
"""
print(cadenota)


//tuplas
let coordenadas: (Int,Int,Int) = (3,2,1)
coordenadas.0

let coords = (4,5)
let coordXY : (x: Int, y: Int) = (8,9)
coordXY.x
