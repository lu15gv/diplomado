//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

class Song{
    let title: String
    let artist: String
    var duration: Int
    init(title:String, artist:String, duration:Int) {
        self.title = title
        self.artist = artist
        self.duration = duration
    }
}
let song = Song(title: "Viva la vida", artist: "Coldplay", duration: 3)
song.duration = 4

