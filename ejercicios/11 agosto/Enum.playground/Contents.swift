//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

enum Suit {
    case swords, hearts, diamonds, clubs
    var rank: Int{
        switch self {
        case .swords: return 4
        case .hearts: return 3
        case .diamonds: return 2
        case .clubs: return 1
        }
    }
    func beats(_ otherSuit: Suit) -> Bool{
        return self.rank > otherSuit.rank
    }
}

let mySuit = Suit.swords
mySuit.rank
let yourSuit = Suit.diamonds
mySuit.rank > yourSuit.rank
mySuit.beats(yourSuit)

