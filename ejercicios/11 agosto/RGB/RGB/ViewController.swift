//
//  ViewController.swift
//  RGB
//
//  Created by Gomez Luis on 11/08/18.
//  Copyright © 2018 Gomez Luis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    @IBOutlet weak var redSwitch: UISwitch!
    @IBOutlet weak var greenSwitch: UISwitch!
    @IBOutlet weak var blueSwitch: UISwitch!
    @IBOutlet weak var colorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colorView.layer.cornerRadius = 10
        colorView.layer.borderWidth = 5
        updateViewElements()
    }
    
    func updateViewElements() {
        redSlider.isEnabled = redSwitch.isOn
        greenSlider.isEnabled = greenSwitch.isOn
        blueSlider.isEnabled = blueSwitch.isOn
        let red:CGFloat = redSwitch.isOn ? CGFloat(redSlider.value) : 0
        let green:CGFloat = greenSwitch.isOn ? CGFloat(greenSlider.value) : 0
        let blue:CGFloat = blueSwitch.isOn ? CGFloat(blueSlider.value) : 0
        colorView.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
    
    @IBAction func didChangeValue(_ sender: Any) {
        updateViewElements()
    }
    
    @IBAction func didReset(_ sender: Any) {
        redSlider.value = 1
        greenSlider.value = 1
        blueSlider.value = 1
        redSwitch.isOn = false
        greenSwitch.isOn = false
        blueSwitch.isOn = false
        updateViewElements()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

