//
//  ViewController.swift
//  RockPapperScissors
//
//  Created by Gomez Luis on 11/08/18.
//  Copyright © 2018 Gomez Luis. All rights reserved.
//

import UIKit

enum HandType {
    case rock, papper, scissor
    var stringValue:String {
        switch self {
        case .rock:
            return "👊"
        case .papper:
            return "🖐"
        case .scissor:
            return "✌️"
        }
    }
    func doIWin(against: HandType) -> Bool? {
        switch against {
        case .rock:
            if self == .papper{
                return true
            }else if self == .scissor{
                return false
            }
        case .scissor:
            if self == .papper{
                return false
            }else if self == .rock{
                return true
            }
        case .papper:
            if self == .rock{
                return false
            }else if self == .scissor{
                return true
            }
        }
        return nil
    }
}
    


class ViewController: UIViewController {

    @IBOutlet weak var resultView: UIView!
    @IBOutlet weak var resultLbl: UILabel!
    @IBOutlet weak var myHandChoice: UILabel!
    @IBOutlet weak var randomHandChoice: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resultView.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func played(with handType: HandType) {
        
        let number = Int(arc4random_uniform(3) + 1)
        var randomHand:HandType
        switch number {
        case 1:
            randomHand = .rock
        case 2:
            randomHand = .papper
        case 3:
            randomHand = .scissor
        default:
            randomHand = .rock
        }
        myHandChoice.text = handType.stringValue
        randomHandChoice.text = randomHand.stringValue
        if let result = handType.doIWin(against: randomHand){
            if result{
                resultLbl.text = "You won"
            }else{
                resultLbl.text = "You lost"
            }
        }else{
            resultLbl.text = "Draw"
        }
        resultView.isHidden = false
    }
    @IBAction func didSelectRock(_ sender: Any) {
        played(with: .rock)
    }
    @IBAction func didSelectPapper(_ sender: Any) {
        played(with: .papper)
    }
    @IBAction func didSelectScissor(_ sender: Any) {
        played(with: .scissor)
    }
    @IBAction func didReset(_ sender: Any) {
        resultView.isHidden = true
    }
    

}

